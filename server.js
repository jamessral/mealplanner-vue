const express = require('express');
const serveStatic = require('serve-static');
const path = require('path');
const port = process.env.PORT || 8080;
const app = express();

// the __dirname is the current directory from where the script is running
app.use('/', serveStatic(path.join(__dirname, '/dist')))
app.listen(port);
