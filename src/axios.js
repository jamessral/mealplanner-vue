import axios from 'axios'

const port = process.env.APIPORT || 4000
const baseURL =
  process.env.NODE_ENV === 'development'
    ? `http://localhost:${port}/api/v1`
    : `${process.env.API_URL}/api/v1`

export default axios.create({
  baseURL,
  timeout: 1000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
})
