import 'es6-promise/auto'

import Vue from 'vue'
import Router from 'vue-router'
import IngredientsRoot from '@/components/Ingredients/IngredientsRoot'
import RecipesRoot from '@/components/Recipes/RecipesRoot'
import MealPlans from '@/components/MealPlans/MealPlans'
import SignInRoot from '@/components/Auth/SignInRoot'
import SignUpRoot from '@/components/Auth/SignUpRoot'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/ingredients',
      name: 'ingredients',
      component: IngredientsRoot,
    },
    {
      path: '/recipes',
      name: 'recipes',
      component: RecipesRoot,
    },
    {
      path: '/mealplans',
      name: 'mealplans',
      component: MealPlans,
    },
    {
      path: '/sign_in',
      name: 'signIn',
      component: SignInRoot,
    },
    {
      path: '/sign_up',
      name: 'signUp',
      component: SignUpRoot,
    },
  ],
})
