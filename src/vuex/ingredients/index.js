import axios from '@/axios'
import getAuthHeader from '@/lib/getAuthHeader'

export const initialFormState = {
  name: '',
  description: '',
  price: 0,
}

const mutations = {
  clearForm(state) {
    state.formIngredient = { ...initialFormState }
  },
  load(state) {
    state.loading = true
    state.loadError = null
  },
  loadFail(state, err) {
    state.loading = false
    state.loadError = err
  },
  loadSuccess(state, ingredients) {
    state.loading = false
    state.ingredients = ingredients
  },
  toggleEditing(state) {
    state.editing = !state.editing
  },
  create(state) {
    state.creating = true
    state.createError = null
  },
  createFail(state, err) {
    state.creating = false
    state.createError = err
  },
  createSuccess(state, ingredient) {
    state.creating = false
    state.ingredients.push(ingredient)
  },
  edit(state, { ingredient, id }) {
    state.editing = true
    state.editError = null
    state.editId = id
    state.formIngredient = ingredient
  },
  editFail(state, err) {
    state.editing = false
    state.editError = err
    state.editId = null
  },
  editSuccess(state, { ingredient, id }) {
    const index = state.ingredients.findIndex(ing => ing.id === id)

    state.editing = false
    state.editId = null

    state.ingredients = [
      ...state.ingredients.slice(0, index),
      ingredient,
      ...state.ingredients.slice(index + 1),
    ]
  },
  remove(state) {
    state.removing = true
    state.removeError = null
  },
  removeFail(state, err) {
    state.removing = false
    state.removeError = err
  },
  removeSuccess(state, id) {
    state.removing = false
    state.ingredients = state.ingredients.filter(i => i.id !== id)
  },
}

const actions = {
  beginEdit({ commit }, args) {
    commit('edit', args)
  },
  clearForm({ commit }) {
    commit('clearForm')
  },
  async create({ commit }, ingredient) {
    commit('create')
    try {
      const token = await localStorage.getItem('authToken')
      const { data } = await axios.post('./ingredients', ingredient, {
        headers: getAuthHeader(token),
      })
      commit('createSuccess', data)
      commit('clearForm')
    } catch (err) {
      commit('createFail', err.message)
      commit('clearForm')
    }
  },
  async load({ commit }) {
    commit('load')
    try {
      const token = await localStorage.getItem('authToken')
      const { data } = await axios.get('./ingredients', {
        headers: getAuthHeader(token),
      })
      commit('loadSuccess', data.ingredients)
    } catch (err) {
      commit('loadFail', err.message)
    }
  },
  async remove({ commit }, id) {
    commit('remove')
    try {
      const token = await localStorage.getItem('authToken')
      await axios.delete(`./ingredients/${id}`, {
        headers: getAuthHeader(token),
      })
      commit('removeSuccess', id)
    } catch (err) {
      commit('removeFail', err.message)
    }
  },
  async update({ commit, state }, ingredient) {
    try {
      const token = await localStorage.getItem('authToken')
      const { data } = await axios.put(
        `./ingredients/${state.editId}`,
        ingredient,
        { headers: getAuthHeader(token) }
      )
      await commit('editSuccess', {
        id: state.editId,
        ingredient: data,
      })
      commit('clearForm')
    } catch (err) {
      await commit('editFail', err.message)
      commit('clearForm')
    }
  },
}

const getters = {
  createError: state => state.createError,
  creating: state => state.creating,
  editError: state => state.editError,
  editId: state => state.editId,
  editing: state => state.editing,
  formIngredient: state => state.formIngredient,
  ingredients: state => state.ingredients,
  loadError: state => state.loadError,
  loading: state => state.loading,
  removeError: state => state.removeError,
  removing: state => state.removing,
}

const state = {
  createError: null,
  creating: false,
  editError: null,
  editId: null,
  editing: false,
  formIngredient: { ...initialFormState },
  ingredients: [],
  loadError: null,
  loading: false,
  removeError: null,
  removing: false,
}

export default {
  state,
  mutations,
  actions,
  getters,
}
