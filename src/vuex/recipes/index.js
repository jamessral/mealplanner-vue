import axios from '@/axios'
import getAuthHeader from '@/lib/getAuthHeader'

export const initialFormState = {
  name: '',
  description: '',
  ingredients: [],
}

const mutations = {
  addFormIngredient(state, ingredient) {
    state.formRecipe.ingredients.push(ingredient)
  },
  removeFormIngredient(state, ingredient) {
    state.formRecipe.ingredients = state.formRecipe.ingredients.filter(
      ing => ing.id !== ingredient.id
    )
  },
  clearForm(state) {
    state.formRecipe = {
      ...initialFormState,
      ingredients: [],
    }
  },
  load(state) {
    state.loading = true
    state.loadError = null
  },
  loadFail(state, err) {
    state.loading = false
    state.loadError = err
  },
  loadSuccess(state, recipes) {
    state.loading = false
    state.recipes = recipes
  },
  toggleEditing(state) {
    state.editing = !state.editing
  },
  create(state) {
    state.creating = true
    state.createError = null
  },
  createFail(state, err) {
    state.creating = false
    state.createError = err
  },
  createSuccess(state, recipes) {
    state.creating = false
    state.recipes.push(recipes)
  },
  edit(state, { recipe, id }) {
    state.editing = true
    state.editError = null
    state.editId = id
    state.formRecipe = recipe
  },
  editFail(state, err) {
    state.editing = false
    state.editError = err
    state.editId = null
  },
  editSuccess(state, { recipe, id }) {
    const index = state.recipes.findIndex(ing => ing.id === id)

    state.editing = false
    state.editId = null

    state.recipes = [
      ...state.recipes.slice(0, index),
      recipe,
      ...state.recipes.slice(index + 1),
    ]
  },
  getAvailable(state) {
    state.gettingAvailable = true
    state.gettingAvailableError = null
  },
  getAvailableFail(state, error) {
    state.gettingAvailable = false
    state.gettingAvailableError = error
  },
  getAvailableSuccess(state, ingredients) {
    state.gettingAvailable = false
    state.availableIngredients = ingredients
  },
  remove(state) {
    state.removing = true
    state.removeError = null
  },
  removeFail(state, err) {
    state.removing = false
    state.removeError = err
  },
  removeSuccess(state, id) {
    state.removing = false
    state.recipes = state.recipes.filter(i => i.id !== id)
  },
}

const actions = {
  addFormIngredient({ commit }, recipe) {
    commit('addFormIngredient', recipe)
  },
  removeFormIngredient({ commit }, recipe) {
    commit('removeFormIngredient', recipe)
  },
  beginEdit({ commit }, args) {
    commit('edit', args)
  },
  clearForm({ commit }) {
    commit('clearForm')
  },
  async create({ commit }, recipe) {
    commit('create')
    try {
      const token = await localStorage.getItem('authToken')
      const { data } = await axios.post('./recipes', recipe, {
        headers: getAuthHeader(token),
      })
      commit('createSuccess', data)
      commit('clearForm')
    } catch (err) {
      commit('createFail', err.message)
      commit('clearForm')
    }
  },
  async getAvailableIngredients({ commit }, id) {
    commit('getAvailable')
    try {
      const token = await localStorage.getItem('authToken')
      const { data } = await axios.post(
        `./recipes/${id}/availableIngredients`,
        {
          headers: getAuthHeader(token),
        }
      )
      commit('getAvailableSuccess', data.ingredients)
    } catch (err) {
      commit('getAvailableFail', err.message)
    }
  },
  async load({ commit }) {
    commit('load')
    try {
      const token = await localStorage.getItem('authToken')
      const { data } = await axios.get('./recipes', {
        headers: getAuthHeader(token),
      })
      commit('loadSuccess', data.recipes)
    } catch (err) {
      commit('loadFail', err.message)
    }
  },
  async remove({ commit }, id) {
    commit('remove')
    try {
      const token = await localStorage.getItem('authToken')
      await axios.delete(`./recipes/${id}`, { headers: getAuthHeader(token) })
      commit('removeSuccess', id)
    } catch (err) {
      commit('removeFail', err.message)
    }
  },
  async update({ commit, state }, recipe) {
    try {
      const token = await localStorage.getItem('authToken')
      const { data } = await axios.put(`./recipes/${state.editId}`, recipe, {
        headers: getAuthHeader(token),
      })
      await commit('editSuccess', {
        id: state.editId,
        recipe: data,
      })
      commit('clearForm')
    } catch (err) {
      await commit('editFail', err.message)
      commit('clearForm')
    }
  },
}

const getters = {
  availableIngredients: state => state.availableIngredients,
  createError: state => state.createError,
  creating: state => state.creating,
  editError: state => state.editError,
  editId: state => state.editId,
  editing: state => state.editing,
  formRecipe: state => state.formRecipe,
  recipes: state => state.recipes,
  loadError: state => state.loadError,
  loading: state => state.loading,
  removeError: state => state.removeError,
  removing: state => state.removing,
}

const state = {
  availableIngredients: [],
  createError: null,
  creating: false,
  editError: null,
  editId: null,
  editing: false,
  gettingAvailable: false,
  gettingAvailableError: null,
  formRecipe: { ...initialFormState },
  recipes: [],
  loadError: null,
  loading: false,
  removeError: null,
  removing: false,
}

export default {
  state,
  mutations,
  actions,
  getters,
}
