import Vue from 'vue'
import Vuex from 'vuex'
import ingredientsModule from '@/vuex/ingredients'
import recipesModule from '@/vuex/recipes'
import usersModule from '@/vuex/users'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    ingredients: {
      namespaced: true,
      ...ingredientsModule,
    },
    recipes: {
      namespaced: true,
      ...recipesModule,
    },
    users: {
      namespaced: true,
      ...usersModule,
    },
  },
})
