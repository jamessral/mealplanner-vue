import axios from '@/axios'
import router from '@/router'

const mutations = {
  login(state) {
    state.loggingIn = true
    state.loginError = null
  },
  loginFail(state, error) {
    state.loggingIn = false
    state.loginError = error
  },
  loginSuccess(state, { token, email }) {
    state.email = email
    state.loggingIn = false
    state.authToken = token
  },
  logout(state) {
    state.authToken = null
    state.email = null
    state.isLoggedIn = true
  },
  signUp(state) {
    state.signingUp = true
    state.signUpError = null
  },
  signUpFail(state, error) {
    state.signingUp = false
    state.signUpError = error
  },
  signUpSuccess(state) {
    state.signingUp = false
  },
}

const actions = {
  async login({ commit }, { email, password }) {
    commit('login')
    try {
      const { data } = await axios.post('./users/signIn', { email, password })
      commit('loginSuccess', { token: data.token, email })
      await localStorage.setItem('authToken', data.token)
      router.push('/')
    } catch (err) {
      commit('loginFail', err.message)
    }
  },
  async logout({ commit }) {
    await localStorage.setItem('authToken', '')
    commit('logout')
    router.push('/')
  },
  async signUp({ commit }, { email, password, passwordConfirmation }) {
    commit('signUp')
    try {
      await axios.post('./users/signUp', {
        email,
        password,
        passwordConfirmation,
      })
      commit('signUpSuccess')
    } catch (err) {
      commit('signUpFail', err.message)
    }
  },
}

export const getters = {
  authToken: state => state.authToken,
  email: state => state.email,
  isLoggedIn: state => Boolean(state.authToken),
  loggingIn: state => state.loggingIn,
  loginError: state => state.loginError,
  signingUp: state => state.signingUp,
  signUpError: state => state.signUpError,
}

const state = {
  authToken: localStorage.getItem('authToken') || null,
  loggingIn: false,
  loginError: null,
  email: null,
  signingUp: false,
  signUpError: null,
}

export default {
  mutations,
  actions,
  getters,
  state,
}
