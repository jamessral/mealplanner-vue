import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import NavBar from '@/components/common/NavBar/NavBar'
import { getters } from '@/vuex/users'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('NavBar.vue', () => {
  let actions
  let store

  beforeEach(() => {
    actions = {
      'users/logout': jest.fn(),
    }
    store = new Vuex.Store({
      modules: {
        users: {
          state: {
            users: {
              isLoggedIn: false,
            },
          },
          actions,
          getters,
        },
      },
    })
  })

  it('should render correct contents', () => {
    const wrapper = shallowMount(NavBar, {
      stubs: ['router-link'],
      store,
      localVue,
    })

    expect(wrapper.find('span').text()).toEqual('MealPlanner')
  })
})
